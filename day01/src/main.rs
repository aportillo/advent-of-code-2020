use helper;
use std::collections::HashMap;

fn main() {
    let question_part = helper::get_question_part();
    let input = helper::read_file_to_string_vec("input.txt", env!("CARGO_PKG_NAME"));
    let answer: i32;
    match question_part {
        helper::QuestionPart::Part1 => {
            answer = repair_report(&input, 2020).unwrap();
        }
        helper::QuestionPart::Part2 => {
            answer = repair_report_thrice(&input, 2020).unwrap();
        }
    }
    println!(
        "The answer to {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Return product of two numbers whose sum is given by sum.
fn repair_report(values: &Vec<String>, sum: i32) -> Result<i32, String> {
    let mut map = HashMap::new();
    for value in values {
        let value: i32 = value.to_string().parse().unwrap();
        if map.contains_key(&(sum - value)) {
            return Ok(value * &(sum - value));
        }
        map.insert(value, 0);
    }
    return Err(format!("Could not find two numbers that sum to {}.", sum));
}

/// Return product of three numbers whose sum is given by sum.
fn repair_report_thrice(values: &Vec<String>, sum: i32) -> Result<i32, String> {
    for value in values {
        let value: i32 = value.to_string().parse().unwrap();
        if let Ok(inner_product) = repair_report(values, sum - value) {
            return Ok(value * inner_product);
        }
    }
    return Err(format!("Could not find three numbers that sum to {}.", sum));
}
