use helper;

fn main() {
    let question_part = helper::get_question_part();
    let input = helper::read_file_to_string_vec("input.txt", env!("CARGO_PKG_NAME"));
    let answer: u32;
    match question_part {
        helper::QuestionPart::Part1 => {
            answer = valid_passwords(&input);
        }
        helper::QuestionPart::Part2 => {
            answer = valid_passwords_2(&input);
        }
    }
    println!(
        "The answer to {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Count number of valid passwords.
fn valid_passwords(input: &Vec<String>) -> u32 {
    let mut valid_password_count: u32 = 0;
    for line in input {
        let parts = split_line(line);
        let count_requirement = character_count_requirement(parts[0]);
        let character = get_character(parts[1]);
        let password = parts[2];
        let count = get_character_count(password, character);
        if count >= count_requirement[0] && count <= count_requirement[1] {
            valid_password_count += 1;
        }
    }
    valid_password_count
}

/// Count number of valid passwords for part 2.
fn valid_passwords_2(input: &Vec<String>) -> u32 {
    let mut valid_password_count: u32 = 0;
    for line in input {
        let parts = split_line(line);
        let count_requirement = character_count_requirement(parts[0]);
        let character = get_character(parts[1]);
        let password = parts[2];

        let first_character: char = match get_nth_character(password, count_requirement[0]) {
            Some(value) => value,
            None => continue,
        };
        let second_character: char = match get_nth_character(password, count_requirement[1]) {
            Some(value) => value,
            None => continue,
        };

        if !(first_character == character && second_character == character)
            && (first_character == character || second_character == character)
        {
            valid_password_count += 1;
        }
    }
    valid_password_count
}

/// Split lines for further processing.
fn split_line(line: &String) -> Vec<&str> {
    let split = line.split_whitespace();
    split.collect::<Vec<_>>()
}

/// Process first part of password policy.
fn character_count_requirement(range: &str) -> [u16; 2] {
    let mut split = range.split("-");
    [
        split.next().unwrap().parse::<u16>().unwrap(),
        split.next().unwrap().parse::<u16>().unwrap(),
    ]
}

// Get character to check in password.
fn get_character(password_str: &str) -> char {
    password_str.chars().next().unwrap()
}

/// Return number of specific character in string.
fn get_character_count(characters: &str, character: char) -> u16 {
    characters.matches(character).count() as u16
}

// Get nth character using 1-index instead of 0-index.
fn get_nth_character(characters: &str, n: u16) -> Option<char> {
    match characters.chars().nth(n as usize - 1) {
        Some(value) => Some(value),
        None => None,
    }
}

#[test]
fn test_split_line() {
    let v = vec!["1-3", "a:", "abcde"];
    assert_eq!(split_line(&"1-3 a: abcde".to_string()), v)
}

#[test]
fn test_character_count() {
    let v = [1, 3];
    assert_eq!(character_count_requirement(&"1-3"), v);
}

#[test]
fn test_get_character() {
    let c: char = 'a';
    assert_eq!(get_character(&"a:"), c);
}

#[test]
fn test_valid_passwords() {
    let input = helper::read_file_to_string_vec("test-input.txt", "../day02");
    assert_eq!(valid_passwords(&input), 2)
}

#[test]
fn test_valid_passwords_2() {
    let input = helper::read_file_to_string_vec("test-input.txt", "../day02");
    assert_eq!(valid_passwords_2(&input), 1)
}

#[test]
fn test_get_character_count() {
    let c = "ccccccccc";
    assert_eq!(get_character_count(c, 'c'), 9);
}

#[test]
fn test_get_nth_character_success() {
    assert_eq!(get_nth_character("abcd", 2), Some('b'))
}

#[test]
fn test_get_nth_character_failure() {
    assert_eq!(get_nth_character("abcd", 10), None)
}
