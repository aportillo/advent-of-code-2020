use helper;

#[derive(Debug, PartialEq)]
struct Position {
    row: u32,
    column: u32,
}

struct Slope {
    rise: u32,
    run: u32,
}

fn main() {
    let question_part = helper::get_question_part();
    let input = helper::read_file_to_string_vec("input.txt", env!("CARGO_PKG_NAME"));
    let mut answer: u32;
    match question_part {
        helper::QuestionPart::Part1 => {
            answer = count_trees_encountered(&input, Slope { rise: 1, run: 3 });
        }
        helper::QuestionPart::Part2 => {
            let slopes = get_slopes();
            answer = 1;
            for slope in slopes {
                answer *= count_trees_encountered(&input, slope);
            }
        }
    }
    println!(
        "The answer to {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Count total rows in forest.
fn count_total_rows(input: &Vec<String>) -> u32 {
    input.len() as u32
}

/// Count total columns in forest.
fn count_total_columns(input: &Vec<String>) -> u32 {
    input[0].len() as u32
}

/// Count number of trees encountered in forest by following provided slope.
fn count_trees_encountered(input: &Vec<String>, slope: Slope) -> u32 {
    let total_rows = count_total_rows(&input);
    let mut position = Position { row: 1, column: 1 };
    let mut tree_count: u32 = 0;

    while position.row < total_rows {
        position = step_down(position, &slope);
        let row = get_row(&input, position.row);
        let column = get_repeated_column(&input, position.column);

        if get_nth_character(row, column).unwrap() == '#' {
            tree_count += 1;
        }
    }
    tree_count
}

/// Step down cursor by provided row and column values.
fn step_down(mut position: Position, slope: &Slope) -> Position {
    position.row = position.row + slope.rise;
    position.column = position.column + slope.run;
    position
}

/// Return forest row.
fn get_row(input: &Vec<String>, row: u32) -> &String {
    &input[row as usize - 1]
}

/// Return column number taking into account forest may need to be repeated
/// if not wide enough.
fn get_repeated_column(input: &Vec<String>, column: u32) -> u32 {
    let total_columns = count_total_columns(&input);
    if column % total_columns == 0 {
        total_columns
    } else {
        column % total_columns
    }
}

// Get nth character using 1-index instead of 0-index.
fn get_nth_character(characters: &str, n: u32) -> Option<char> {
    match characters.chars().nth((n - 1) as usize) {
        Some(value) => Some(value),
        None => None,
    }
}

/// Create list of slopes used for part 2.
fn get_slopes() -> Vec<Slope> {
    let mut slopes: Vec<Slope> = Vec::new();
    slopes.push(Slope { rise: 1, run: 1 });
    slopes.push(Slope { rise: 1, run: 3 });
    slopes.push(Slope { rise: 1, run: 5 });
    slopes.push(Slope { rise: 1, run: 7 });
    slopes.push(Slope { rise: 2, run: 1 });
    slopes
}

#[test]
fn test_count_rows() {
    let input = helper::read_file_to_string_vec("test-input.txt", "../day03");
    assert_eq!(count_total_rows(&input), 11);
}

#[test]
fn test_count_columns() {
    let input = helper::read_file_to_string_vec("test-input.txt", "../day03");
    assert_eq!(count_total_rows(&input), 11);
}

#[test]
fn test_step_down() {
    let position = Position { row: 1, column: 1 };
    let slope = Slope { rise: 1, run: 3 };
    assert_eq!(step_down(position, &slope), Position { row: 2, column: 4 })
}
#[test]
fn test_get_row() {
    let input = helper::read_file_to_string_vec("test-input.txt", "../day03");
    assert_eq!(get_row(&input, 1), "..##.......")
}

#[test]
fn test_repeated_column() {
    let input = helper::read_file_to_string_vec("test-input.txt", "../day03");
    assert_eq!(get_repeated_column(&input, 15), 4)
}
