use helper;
use std::collections::HashMap;

fn main() {
    let question_part = helper::get_question_part();
    let input = helper::read_file_to_string("input.txt", env!("CARGO_PKG_NAME"));
    let answer: u32;
    match question_part {
        helper::QuestionPart::Part1 => {
            answer = count_valid_passports(&input, &question_part);
        }
        helper::QuestionPart::Part2 => {
            answer = count_valid_passports(&input, &question_part);
        }
    }
    println!(
        "The answer to {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Count valid passports.
fn count_valid_passports(input: &String, question_part: &helper::QuestionPart) -> u32 {
    let passports = split_passports(&input);
    let mut valid_passports: u32 = 0;
    for passport in passports {
        match question_part {
            helper::QuestionPart::Part1 => {
                if validate_passport(&split_fields(passport)) {
                    valid_passports += 1;
                }
            }
            helper::QuestionPart::Part2 => {
                if validate_passport_fields(&split_fields(passport)) {
                    valid_passports += 1;
                }
            }
        }
    }
    valid_passports
}

/// Separate individual passports from input text and remove newline characters.
fn split_passports(input: &String) -> Vec<String> {
    let passports = input.split("\n\n").collect::<Vec<&str>>();
    let mut single_line_passports: Vec<String> = Vec::new();
    for passport in passports {
        single_line_passports.push(passport.replace("\n", " "));
    }
    single_line_passports
}

/// Create hashmap of passport fields.
fn split_fields(passport: String) -> HashMap<String, String> {
    let fields: Vec<&str> = passport.split(" ").collect();
    let mut passport_map = HashMap::new();
    for field in fields {
        let mut split = field.split(":");
        let key = split.next().unwrap().to_owned();
        let value = split.next().unwrap().to_owned();
        passport_map.insert(key, value);
    }
    passport_map
}

/// Check if passport has all required fields.
fn validate_passport(fields: &HashMap<String, String>) -> bool {
    let required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    required_fields
        .iter()
        .all(|x| fields.contains_key(x.to_owned()))
}

/// Check if passport has all required fields.
fn validate_passport_fields(fields: &HashMap<String, String>) -> bool {
    let required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    // Keep track of which individual fields are valid.
    let mut valid_fields: Vec<bool> = Vec::new();

    // Check if all fields are present before validating individual fields.
    if !validate_passport(fields) {
        return false;
    }
    for field in &required_fields {
        let value = fields.get(field.to_owned()).unwrap();
        match field.as_ref() {
            "byr" => valid_fields.push(validate_range(value, 1920, 2002)),
            "iyr" => valid_fields.push(validate_range(value, 2010, 2020)),
            "eyr" => valid_fields.push(validate_range(value, 2020, 2030)),
            "hgt" => valid_fields.push(validate_height(value)),
            "hcl" => valid_fields.push(validate_hair_colour(value)),
            "ecl" => valid_fields.push(validate_eye_colour(value)),
            "pid" => valid_fields.push(validate_passport_id(value)),
            _ => continue,
        }
    }

    // Return true if all individual fields valid.
    valid_fields.iter().all(|x| *x)
}

// Given a value, return whether value falls between the given min/max.
fn validate_range(value: &String, min: u32, max: u32) -> bool {
    let value = value.parse::<u32>().unwrap();
    return if value >= min && value <= max {
        true
    } else {
        false
    };
}

// Given a height, parse out units and check whether height is valid.
fn validate_height(height: &String) -> bool {
    let len = height.len();
    let units = &height[len - 2..];
    let value = &height[..len - 2];
    match units {
        "in" => validate_range(&value.to_string(), 59, 76),
        "cm" => validate_range(&value.to_string(), 150, 193),
        _ => return false,
    }
}

/// Check whether hair colour is valid hex colour.
fn validate_hair_colour(colour: &String) -> bool {
    let first = colour.chars().next().unwrap();
    let hex = &colour[1..];
    let is_all_hex = hex.chars().all(|x| x.is_ascii_hexdigit());
    return if first == '#' && hex.len() == 6 as usize && is_all_hex {
        true
    } else {
        false
    };
}

/// Check whether eye colour is one of allowed colours.
fn validate_eye_colour(colour: &String) -> bool {
    let colours = colour;
    match colours.as_ref() {
        "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => return true,
        _ => return false,
    }
}

/// Check whether passport id is digit (0-9) and has a length of 9.
fn validate_passport_id(id: &String) -> bool {
    let is_digit = id.chars().all(|x| x.is_ascii_digit());
    return if id.len() == 9 as usize && is_digit {
        true
    } else {
        false
    };
}
