use helper;

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug)]
struct Limits {
    upper: u32,
    lower: u32,
}

#[derive(Debug, Eq, Ord, PartialOrd)]
struct Seat {
    row: u32,
    column: u32,
}

impl PartialEq for Seat {
    fn eq(&self, other: &Self) -> bool {
        self.row == other.row && self.column == other.column
    }
}

fn main() {
    let question_part = helper::get_question_part();
    let input = helper::read_file_to_string_vec("input.txt", env!("CARGO_PKG_NAME"));
    let answer: u32;
    let taken_seats = get_taken_seats(input);

    match question_part {
        helper::QuestionPart::Part1 => answer = calculate_highest_seat_id(taken_seats),
        helper::QuestionPart::Part2 => {
            answer = find_seat(taken_seats);
        }
    }
    println!(
        "The answer to {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Create vector of all seats taken by passengers.
fn get_taken_seats(boarding_passes: Vec<String>) -> Vec<Seat> {
    let mut taken_seats: Vec<Seat> = Vec::new();
    for pass in boarding_passes {
        taken_seats.push(Seat {
            row: get_seat(
                pass[..7].to_string(),
                Limits {
                    upper: 127,
                    lower: 0,
                },
            ),
            column: get_seat(pass[7..].to_string(), Limits { upper: 7, lower: 0 }),
        });
    }
    taken_seats
}

/// Find your seat. All seats except yours are filled. Your seat was not at the very front or back.
fn find_seat(taken_seats: Vec<Seat>) -> u32 {
    let mut taken_seats = taken_seats;
    taken_seats.sort();
    taken_seats.iter();
    let mut previous_seat_id: u32 = 0;
    for seat in &taken_seats {
        let current_seat_id = calculate_seat_id(seat);
        if current_seat_id - previous_seat_id == 2 {
            return current_seat_id - 1;
        }
        previous_seat_id = current_seat_id;
    }
    panic!("Could not find your seat.");
}

/// Calculate highest seat ID.
fn calculate_highest_seat_id(seats: Vec<Seat>) -> u32 {
    let mut highest_seat_id: u32 = 0;

    for seat in seats {
        let seat_id = calculate_seat_id(&seat);
        if seat_id > highest_seat_id {
            highest_seat_id = seat_id;
        }
    }
    highest_seat_id
}

/// Calculate seat ID, which is row * 8 + column.
fn calculate_seat_id(seat: &Seat) -> u32 {
    seat.row * 8 + seat.column
}

/// Convert 10 characters to seat row and column.
fn get_seat(instructions: String, limits: Limits) -> u32 {
    match instructions.chars().next() {
        Some(instruction) => {
            let instruction = convert_instruction(instruction);
            // Case where last instruction has been reached.
            if limits.upper - limits.lower == 1 {
                match instruction {
                    Direction::Up | Direction::Right => {
                        return limits.upper;
                    }
                    Direction::Down | Direction::Left => {
                        return limits.lower;
                    }
                };
            };
            // Otherwise keep recursing through instructions.
            let limits = update_limits(instruction, limits);
            get_seat(instructions[1..].to_string(), limits)
        }
        None => panic!("Went too far!"),
    }
}

// Convert char to instruction enum.
fn convert_instruction(letter: char) -> Direction {
    match letter {
        'B' => Direction::Up,
        'F' => Direction::Down,
        'R' => Direction::Right,
        'L' => Direction::Left,
        _ => panic!("Incorrect instruction found!"),
    }
}

/// Update the upper and lower limits when dividing in half.
fn update_limits(direction: Direction, limits: Limits) -> Limits {
    match direction {
        Direction::Up | Direction::Right => Limits {
            upper: limits.upper,
            lower: (limits.upper - limits.lower + 1) / 2 + limits.lower,
        },
        Direction::Down | Direction::Left => Limits {
            upper: limits.upper - (limits.upper - limits.lower + 1) / 2,
            lower: limits.lower,
        },
    }
}
