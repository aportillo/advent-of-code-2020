use helper::{self, QuestionPart};
use std::collections::HashMap;

fn main() {
    let question_part = helper::get_question_part();
    let input = helper::read_file_to_string_vec("input.txt", env!("CARGO_PKG_NAME"));
    let answer: u32;
    match question_part {
        QuestionPart::Part1 => answer = count_unique_answers(&input),
        QuestionPart::Part2 => answer = count_total_common_answers(&input),
    }
    println!(
        "The answer to {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    )
}

/// Count unique answers for each group and return total unique answers.
fn count_unique_answers(input: &Vec<String>) -> u32 {
    let mut group_map: HashMap<char, u32> = HashMap::new();
    let mut total: u32 = 0;
    for line in input {
        match line.chars().next() {
            Some(_) => {
                // Add person's answers to group map of unique answers.
                group_map.extend(person_map(line));
            }
            None => {
                total += group_map.keys().len() as u32;
                group_map.clear();
            }
        }
    }
    total
}

/// Count total answers provided by everyone for all groups.
fn count_total_common_answers(input: &Vec<String>) -> u32 {
    let mut people_in_group = 0;
    let mut group_map: HashMap<char, u32> = HashMap::new();
    let mut total = 0;
    for line in input {
        match line.chars().next() {
            Some(_) => {
                group_map = update_group_map(line, group_map);
                people_in_group += 1;
            }
            None => {
                total += count_group_common_answers(&group_map, &people_in_group);
                people_in_group = 0;
                group_map.clear();
            }
        }
    }
    total
}

/// Create map of person's answers.
fn person_map(line: &String) -> HashMap<char, u32> {
    let mut map: HashMap<char, u32> = HashMap::new();
    for letter in line.chars() {
        match map.get(&letter) {
            Some(_) => (),
            None => {
                map.insert(letter, 1);
            }
        }
    }
    map
}

/// Create map of count for each answer provided by the group.
fn update_group_map(line: &String, mut group_map: HashMap<char, u32>) -> HashMap<char, u32> {
    for letter in line.chars() {
        match group_map.get(&letter) {
            Some(value) => {
                group_map.insert(letter, value + 1);
            }
            None => {
                group_map.insert(letter, 1);
            }
        }
    }
    group_map
}

/// Count total number of questions answered by everyone in single group.
/// Add to count if all members of the group answered the question. Calculated by
/// comparing number of answers to number or people in group.
fn count_group_common_answers(map: &HashMap<char, u32>, people_in_group: &u32) -> u32 {
    let mut count = 0;
    for (_, value) in map.iter() {
        if value == people_in_group {
            count += 1;
        }
    }
    count
}
