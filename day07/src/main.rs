use helper::{self, QuestionPart};

#[derive(Debug)]
struct Bag {
    colour: String,
    count: u32,
    contents: Vec<Bag>,
}

fn main() {
    let question_part = helper::get_question_part();
    let input = helper::read_file_to_string_vec("input.txt", env!("CARGO_PKG_NAME"));
    let answer: u32;
    match question_part {
        QuestionPart::Part1 => {
            let bags = parse_input(&input);
            answer = count_bags(&bags, "shiny gold");
        }
        QuestionPart::Part2 => answer = 0,
    }
    println!(
        "The answer to {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    )
}

/// Parse instructions to vector made up of Bag structs.
fn parse_input(input: &Vec<String>) -> Vec<Bag> {
    let mut parsed_instructions: Vec<Bag> = Vec::new();
    for line in input {
        // Get parent bag colour.
        let mut split = line.split(" bags contain ");
        let parent_bag_colour = split.next().unwrap();

        // Split child bags to handle separately.
        let children_bags = split.next().unwrap().split(", ").collect::<Vec<&str>>();

        let mut children_bags_vector: Vec<Bag> = Vec::new();
        for bag in children_bags {
            let mut child_split = bag.splitn(2, " ");

            // Get child bag count.
            let mut count: u32 = 0;
            match child_split.next().unwrap().parse::<u32>() {
                Ok(value) => count = value,
                _ => {}
            }

            // Get child bag colour.
            let colour = child_split.next().unwrap().split(" bag").next().unwrap();

            // Define child bag struct.
            if count != 0 || colour != "other" {
                children_bags_vector.push(Bag {
                    count,
                    colour: colour.to_string(),
                    contents: Vec::new(),
                })
            }
        }

        // Define parent + children struct.
        parsed_instructions.push(Bag {
            count: 1,
            colour: parent_bag_colour.to_string(),
            contents: children_bags_vector,
        })
    }
    parsed_instructions
}

/// Count how many bags can hold target bag colour.
fn count_bags(bags: &Vec<Bag>, target: &str) -> u32 {
    let mut valid_colours: Vec<&str> = Vec::new();
    let mut continue_looping = true;
    let mut colours_to_check: Vec<&str> = Vec::new();
    colours_to_check.push(target);

    while continue_looping {
        let mut new_colours_to_check: Vec<&str> = Vec::new();
        for bag in bags {
            for child_bag in &bag.contents {
                // Check if parent bag has children bags that can hold any of colours_to_check.
                // colours_to_check is list of colours already confirmed to hold target
                // directly or indirectly.
                if colours_to_check.iter().any(|&i| i == &child_bag.colour) {
                    new_colours_to_check.push(&bag.colour);
                }
            }
        }

        // Remove duplicates and colours that have been searched for already.
        new_colours_to_check.dedup();
        for colour in &valid_colours {
            new_colours_to_check.retain(|x| x != colour)
        }

        // Add newly found bags that can hold target.
        valid_colours.extend(&new_colours_to_check);
        if new_colours_to_check.len() == 0 {
            continue_looping = false;
        }
        colours_to_check = new_colours_to_check;
    }

    valid_colours.len() as u32
}
