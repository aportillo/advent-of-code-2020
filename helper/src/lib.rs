use std::env;
use std::fs;
use std::{
    fs::File,
    io::{prelude::*, BufReader},
};

#[derive(Debug)]
pub enum QuestionPart {
    Part1,
    Part2,
}

pub fn get_question_part() -> QuestionPart {
    match env::args()
        .nth(1)
        .expect("Missing question part argument.")
        .as_str()
    {
        "1" => return QuestionPart::Part1,
        "2" => return QuestionPart::Part2,
        _ => panic!("Not a valid question part."),
    };
}

/// Reads input text file and returns vector composed of each line as a string.
/// # Arguments
/// * `file_name` - Name of file to read. Should be located in package root directory.
pub fn read_file_to_string_vec(file_name: &str, package_name: &str) -> Vec<String> {
    let file_path = format!("{}/{}", package_name, file_name);
    let file = File::open(file_path).expect("No such file.");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line."))
        .collect()
}

/// Reads input text file and returns String.
/// # Arguments
/// * `file_name` - Name of file to read. Should be located in package root directory.
/// * `package_name` - Name of package to read file input file from.
pub fn read_file_to_string(file_name: &str, package_name: &str) -> String {
    let file_path = format!("{}/{}", package_name, file_name);
    fs::read_to_string(file_path).unwrap()
}
